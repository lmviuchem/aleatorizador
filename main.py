from aleatoriador import aleatoriador
from ordenador import ordenador

miAleatoriador = aleatoriador()
miOrdenador = ordenador()

miAleatoriador.generarListaAleatoria()
miLista = miAleatoriador.getLista()
miLista = miOrdenador.ordenarLista(miLista)
miOrdenador.imprimirLista(miLista)